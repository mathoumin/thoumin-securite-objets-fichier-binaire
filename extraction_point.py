import sys

chemin_fichier_hexa = str(sys.argv[1])
chemin_fichier_csv = str(sys.argv[2])

fichier_hexa = open(chemin_fichier_hexa, "rb")
fichier_csv = open(chemin_fichier_csv, "w")

#On va se placer au début de la première position GPS
fichier_hexa.read(610)

for i in range(0,50): #503
    #latitue
    val = fichier_hexa.read(4)
    val_str = str(int.from_bytes(val, 'little'))
    val_str_virgule = val_str[:2] + '.' + val_str[2:]
    fichier_csv.write(val_str_virgule)
    fichier_csv.write(",")
    
    #longitude
    val = fichier_hexa.read(4)
    val_str = str(int.from_bytes(val, 'little'))
    val_str_virgule = val_str[:2] + '.' + val_str[2:]
    fichier_csv.write(val_str_virgule)
    fichier_csv.write("\n")
    
    #saut jusqu'au prochain point
    if i % 2 == 0:
        fichier_hexa.read(41)
    else:
        fichier_hexa.read(59)
    #val = fichier_hexa.read(10)
    #print(val)
    
 
fichier_hexa.close()

#26